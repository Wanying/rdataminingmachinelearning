'
We explore the use of the valication set approach in order to estimate the test error tates that r
result from fitting various linear models on the Auto datasets

Before we begin, we use the set.seed() function in order to set a dded for R\'s random number generator
so that the reader fo this wook will obtain pricesely the same results as those shown below.

It is generally a good idea to set a random seed when performing an analysis such as corss-validation 
that contains an element of randomness, so that the results obtained can be reproduced precisely at a later time


We begin by using the sample() function to split the set of observations into to halves
by selecting a random subset of 196 observations out of the original 392 observations. 
We refer to these observations as the training set

'

library(ISLR)
set.seed(1)
train=sample(392,196)

'
We then use the usbset option in lm() to fit a linear regression using only the observations 
corresponding to the training set
'
lm.fit=lm(mpg~horsepower,data=Auto,subset=train)

'
We now use the predict() function to setimate the response for all 392 observations, 
and we use the mean() function to calculate the MSE fo the 196 observation in validation set. 
Note that the -train index below selects only the observations that are not in the training dataset
'
attach(Auto)
mean((mpg-predict(lm.fit,Auto))[-train]^2)
'
Therefore, the estimated test MSE for the linear regression fit is 26.14. 
We can use the poly() function to estimate the test error for the polynomial and cubit regression
'

'
If we choose a different training data set instead, we will obtian somewhat different errors on the validation set
'
set.seed(2)
train =sample(392,196)
lm.fit=lm(mpg~horsepower, subset=train)
mean((mpg-predict(lm.fit,Auto))[-train]^2)